FROM php:alpine

WORKDIR /var/www/html

RUN docker-php-ext-install pdo pdo_mysql

CMD ["php","bin/console", "server:run","0.0.0.0:8000"]