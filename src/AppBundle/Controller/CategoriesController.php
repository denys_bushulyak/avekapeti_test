<?php
/**
 * User: denys
 * Date: 19.10.17
 * Time: 22:31
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CategoriesController
 * @package AppBundle\Controller
 * @Route("/categories")
 */
class CategoriesController extends Controller
{
    /**
     * @Route("/", name="categories_list")
     * @Method({"GET"})
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()->getRepository(Category::class);

        $categories = $repository->findAll();

        return $this->render(
            'categories_list.html.twig',
            [
                'categories' => $categories
            ]
        );
    }

    /**
     * @Route("/new", name="new_category")
     * @Method({"GET"})
     */
    public function createAction()
    {
        return $this->render('order/new.html.twig');
    }

    /**
     * @Route("/{id}", name="category_info", requirements={"id":"\d+"})
     * @Method({"GET"})
     */
    public function showCategoryAction($id)
    {
        $repository = $this->getDoctrine()->getRepository(Category::class);

        $category = $repository->find($id);

        if(!$category){
            throw new NotFoundHttpException("Category did not found.");
        }

        return $this->render('category.html.twig',[
            'category'=>$category
        ]);
    }

    /**
     * @Route("/", name="create_category")
     * @Method({"POST"})
     * @param Request $request
     * @return RedirectResponse
     */
    public function postAction(Request $request)
    {
        $category = new Category();

        $category->setTitle($request->get('title'));

        $dm = $this->getDoctrine()->getManager();

        $dm->persist($category);

        $dm->flush();

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/{id}", name="remove_category", requirements={"id":"\d+"})
     * @Method({"DELETE"})
     */
    public function removeAction($id)
    {
        //Better to be will be unlink all products to "n/a" category before.

        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository(Category::class)->find($id);

        if(!$category){
            throw new NotFoundHttpException;
        }

        $em->remove($category);

        $em->flush();

        return new Response();
    }
}