<?php
/**
 * User: denys
 * Date: 19.10.17
 * Time: 22:31
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductsController
 * @package AppBundle\Controller
 * @Route("/products")
 */
class ProductsController extends Controller
{
    /**
     * @Route("/", name="products_list")
     * @Method({"GET"})
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()->getRepository(Product::class);

        $products = $repository->findAll();

        return $this->render(
            'products_list.html.twig',
            [
                'products' => $products,
            ]
        );
    }

    /**
     * @Route("/new", name="create_new_order")
     * @Method({"GET"})
     */
    public function createAction()
    {
        return $this->render('order/new.html.twig');
    }

    /**
     * @Route("/", name="create_product")
     * @Method({"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function postAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository(Category::class)->find($request->get('category_id', false));

        if (!$category) {
            throw new BadRequestHttpException("Category absent.");
        }

        $product = new Product();
        $product->setTitle($request->get('title'));
        $product->setCategory($category);

        $em->persist($product);

        $em->flush();

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/{id}", name="product_info", requirements={"id":"\d+"})
     */
    public function showProductAction($id)
    {
        $repository = $this->getDoctrine()->getRepository(Product::class);

        $product = $repository->find($id);

        if (!$product) {
            throw new NotFoundHttpException("Category did not found.");
        }

        return $this->render(
            'product.html.twig',
            [
                'product' => $product,
            ]
        );
    }
}