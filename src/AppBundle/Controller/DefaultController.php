<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {

        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();

        // replace this example code with whatever you need
        return $this->render(
            'default/index.html.twig',
            [
                'categories' => $categories,
            ]
        );
    }
}
